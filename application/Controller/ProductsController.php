<?php

namespace App\Controller;

use App\Model\ProductsModel;

class ProductsController extends AppController {
    private $products;

    public function __construct()
    {
        parent::__construct();
        $this->products = new ProductsModel();
    }

    public function lister()
    {
        $products = $this->products->lister();
        
        $this->view->render("lister", [
            "products" => $products
        ]);
    }

    public function details($id)
    {
        $product = $this->products->details($id)->fetch_array(MYSQLI_ASSOC);

        // debug($product->fetch_array(MYSQLI_ASSOC)); exit;

        $this->view->render("details", array(
            "nom" => $product['nom'],
            "id" => $product['id'],
            "prix" => $product['prix'],
            "title" => $product['nom']
        ));
    }
}