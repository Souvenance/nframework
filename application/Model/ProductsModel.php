<?php

namespace App\Model;

class ProductsModel extends AppModel {
    public function __construct()
    {
        parent::__construct('Products');
    }

    public function lister()
    {
        $sql = "SELECT * FROM " . $this->table;
        return $this->query($sql);
    }

    public function details($id)
    {
        $id = (int) $id;
        $sql = "SELECT * FROM " . $this->table . 
        " WHERE id = " . $id;
        return $this->query($sql);
    }
}